    <footer class="footer">
        <div class="sub_footer">
            <div class="container clearfix">
                <div class="et-social-icons">
                    <a href="https://www.facebook.com/mmipeace/" target="_blank"><i class="fab fa-facebook-square"  ></i></a>
                    <a href="https://twitter.com/mmipeace100" target="_blank"><i class="fab fa-twitter-square"></i></a>
                    <a href="https://www.instagram.com/mmipeace/" target="_blank"><i class="fab fa-instagram"></i></a>
                    <a href="https://www.youtube.com/mmipeace" target="_blank"><i class="fab fa-youtube-square"></i></a>
                </div>
                <div>COPYRIGHT © 2019 THE MIDDLE WAY: All Rights Reserved . Developed by IBS</div>
            </div>
		</div>
	</footer>