</head>
<body class="">
    <header>
        <nav class="navbar navbar-white bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="#">
                    <img src="/mmiretreat/img/logo-mmipeace-01.png" height="40" alt="logo-mmipeace">
                </a>
            </div>
        </nav>
    </header>
    <div class="et_pb_section et_pb_with_background">
        <div class="et_pb_row et_pb_row_0">
            <div class="col-sm text-center">
                <h1>Retreat Registration</h1>
                <ul class="nav justify-content-center">
                <li class="nav-item">
                    <a class="nav-link" href="#"><small>Home</small></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><small>retreat</small></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="#"><small>retreat registration form</small></a>
                </li>
            </ul>
            </div>
        </div>
    </div>