<?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/doctype.php"); ?>
<?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/css_all.php"); ?>
<?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/navbar.php"); ?>

    <div class="container">
        <div class="row white-space">
            <div class="col-sm">
                <ul class="nav justify-content-center">
                    <li class="nav-item">
                        <a class="nav-link" href="form1.php">Retreat</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="form2.php">Date</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link border-bottom" href="form3.php">Form</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="form4.php">Meditation background</a>
                    </li>
                </ul>
                <h3 class="text-center my-4">Please complete the from. <br>Fields marked with * are required field.</h3>
                <div>
                    <form>
                        <h4 class="my-4">Personal Info:</h4>
                        <div class="form-row">
                            <div class="col">
                                <label for="">First name *</label>
                                <input type="text" class="form-control" placeholder="First name">
                            </div>
                            <div class="col">
                                <label for="">Last name *</label>
                                <input type="text" class="form-control" placeholder="Last name">
                            </div>
                        </div>
                        <div class="form-row my-2">
                            <div class="col">
                                <label>Gender *</label>
                                <div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadioInline1">Male</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadioInline2">Female</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Nationality *</label>
                            <input type="text" class="form-control" id="" placeholder="USA">
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Birth of Date *</label>
                            <input type="text" class="form-control" id="" placeholder="dd-mm-yy">
                        </div>
                        <h4 class="my-4">Contact Info:</h4> 
                        <div class="form-group">
                            <label for="inputAddress">Address *</label>
                            <input type="text" class="form-control" id="" placeholder="Address">
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">City *</label>
                                <input type="text" class="form-control" id="" placeholder="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Province *</label>
                                <input type="text" class="form-control" id="" placeholder="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Country *</label>
                                <input type="text" class="form-control" id="" placeholder="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Zip code *</label>
                                <input type="text" class="form-control" id="" placeholder="">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Email *</label>
                                <input type="email" class="form-control" id="" placeholder="Email">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Phone Number *</label>
                                <input type="text" class="form-control" id="" placeholder="">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Next</button>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
    <?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/footer.php"); ?>
</body>
</html>