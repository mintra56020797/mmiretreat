<?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/doctype.php"); ?>
<?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/css_all.php"); ?>
<?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/navbar.php"); ?>

    <div class="container">
        <div class="row white-space">
            <div class="col-sm">
                <ul class="nav justify-content-center">
                    <li class="nav-item">
                        <a class="nav-link" href="form1.php">Retreat</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="form2.php">Date</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="form3.php">Form</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="form4.php">Meditation background</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link border-bottom" href="form5.php">Health acknowledge</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="form6.php">Payment</a>
                    </li>
                </ul>
                <h3 class="text-center my-4">Please complete the from. <br>Fields marked with * are required field.</h3>
                <p>Please fully complete this form to confirm your health status and please be aware that we cannot take responsibility for providing health care for conditions that we are not made aware of by the applicant. It is advised that you bring ALL medications that you are prescribed with you.</p>
                <div class="card">
                    <div class="card-body">
                        <p class="card-text"><span class="text-danger font-weight-bold">NOTE:</span> Please be advised that all health information given is ‘held’ confidentially by the Middle Way Team and only used to ensure that we are able to provide you with the best care possible.</p>
                    </div>
                </div>
                <div>
                    <form>
                        <h4 class="my-4">Health Acknowledgement</h4>
                        <div class="form-row my-2">
                            <div class="col">
                                <label>How is your health? *</label>
                                <div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadioInline1">Strong</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadioInline2">Passable</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline4" name="customRadioInline1" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadioInline3">Weak</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row my-2">
                            <div class="col">
                                <label>Have you ever had or currently suffer from any of the following health conditions:</label>
                                <div>
                                    <div class="form-group row border-bottom">
                                        <label for="staticEmail" class="col-sm-10 col-form-label">Allergies</label>
                                        <div class="col-sm-2 text-right">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                                <label class="form-check-label" for="inlineRadio1">Yes</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                                <label class="form-check-label" for="inlineRadio2">No</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row border-bottom">
                                        <label for="staticEmail" class="col-sm-10 col-form-label">Heart problems including previous heart attacks</label>
                                        <div class="col-sm-2 text-right">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                                <label class="form-check-label" for="inlineRadio1">Yes</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                                <label class="form-check-label" for="inlineRadio2">No</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row my-2">
                            <div class="col">
                                <label>If any points above is yes, please let us know some detail about that. For example, you allegic with some products, etc.</label>
                                <div>
                                    <textarea class="form-control" id="" rows="3" placeholder="Some description"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-row my-2 mt-5">
                            <div class="col">
                                <label>Do you suffer from any emotional of mental problems, e.g. depression, traumatic stress disorder etc?</label>
                                <div class="">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                        <label class="form-check-label" for="inlineRadio1">Yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                        <label class="form-check-label" for="inlineRadio2">No</label>
                                    </div>
                                </div>
                                <label>Do you take regular prescribed medication?</label>
                                <div class="">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                        <label class="form-check-label" for="inlineRadio1">Yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                        <label class="form-check-label" for="inlineRadio2">No</label>
                                    </div>
                                </div>
                                <label>Do you take alternative health remedies?</label>
                                <div class="">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                        <label class="form-check-label" for="inlineRadio1">Yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                        <label class="form-check-label" for="inlineRadio2">No</label>
                                    </div>
                                </div>
                                <label>Have you had any major accidents?</label>
                                <div class="">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                        <label class="form-check-label" for="inlineRadio1">Yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                        <label class="form-check-label" for="inlineRadio2">No</label>
                                    </div>
                                </div>
                                <label>Have you had any major accidents?</label>
                                <div class="">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                        <label class="form-check-label" for="inlineRadio1">Yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                        <label class="form-check-label" for="inlineRadio2">No</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card my-3">
                            <div class="card-body">
                                <p class="card-text"><span class="text-danger font-weight-bold">IMPORTANT:</span>  The Middle Way Meditation Retreat is Non-Vegetarian Retreat. In case you are vegetarian and would like to join the retreat, we will prepare some vegetarian food for you, please answer the question below and let us know what you cannot eat such as: 
                                    <ul>
                                        <li>eggs</li>
                                        <li>dairy products</li>
                                        <li>fish source, etc</li>
                                    </ul>
                                </p>
                                <label class="font-weight-bold">Would you like a vegetarian or non-vegetarian foods?</label>
                                <div class="">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                        <label class="form-check-label" for="inlineRadio1">Yes</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                        <label class="form-check-label" for="inlineRadio2">No</label>
                                    </div>
                                </div>
                                <textarea class="form-control my-3" id="" rows="3" placeholder="Some description"></textarea>
                            </div>
                        </div>
                        <h4 class="my-4">Health Confirmation Statement *</h4>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                            <label class="custom-control-label" for="customCheck1">
                            <p>I confirm that, I am currently well and able to actively participate in this retreat. If anything happens that might relate to my health due to sudden illness, accident, weather conditions or other situations within the time of the retreat; or while traveling to and from the centre.</p> 
                            <p>I understand I am responsible for myself and will NOT claim or sue The Middle Way Meditation Retreat Program or Foundation. I confirm that the information given in this form is completed, true and correct.</p>
                            <p>Staff member will ask you to sign your name on a copy of your Health Acknowledgement Form in order to validate that all details are correct at the registration counter.</p></label>
                        </div>
                        <button type="submit" class="btn btn-primary">Next</button>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
    <?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/footer.php"); ?>
</body>
</html>