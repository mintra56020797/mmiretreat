<?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/doctype.php"); ?>
<?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/css_all.php"); ?>
<?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/navbar.php"); ?>

    <div class="container">
        <div class="row white-space">
            <div class="col-sm">
                <ul class="nav justify-content-center">
                    <li class="nav-item">
                        <a class="nav-link" href="form1.php">Retreat</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="form2.php">Date</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="form3.php">Form</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link border-bottom" href="form4.php">Meditation background</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="form5.php">Health acknowledge</a>
                    </li>
                </ul>
                <h3 class="text-center my-4">Please complete the from. <br>Fields marked with * are required field.</h3>
                <div>
                    <form>
                        <h4 class="my-4">Meditation Experience</h4>
                        <div class="form-row my-2">
                            <div class="col">
                                <label>Have you ever meditate before? *</label>
                                <div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadioInline1">Yes</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadioInline2">No</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row my-2">
                            <div class="col">
                                <label>Do you have any meditation experiences (please describe)?</label>
                                <div>
                                    <textarea class="form-control" id="" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-inline">
                            <div class="form-group col-md-6">
                                <label class="my-1 mr-4" for="">I have meditated</label>
                                <select class="form-control my-1 mr-sm-2">
                                    <option>All the times</option>
                                    <option>Everyday</option>
                                    <option>Ever other day</option>
                                    <option>Once a week</option>
                                    <option>Occasionally</option>
                                    <option>Rarely</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="my-1 mr-4" for="">Since</label>
                                <select class="form-control my-1 mr-sm-2">
                                    <option>2005</option>
                                    <option>2006</option>
                                    <option>2007</option>
                                    <option>2008</option>
                                    <option>2009</option>
                                    <option>2010</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row my-2">
                            <div class="col">
                                <label>What is your main goal of joining the retreat? *</label>
                                <div>
                                    <textarea class="form-control" id="" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-row my-2">
                            <div class="col">
                                <label>How did you find the Middle Way Retreat? *</label>
                                <select class="form-control">
                                    <option>Word of mouth</option>
                                    <option>Google Search</option>
                                    <option>Youtube</option>
                                    <option>Facebook</option>
                                    <option>Instagram</option>
                                    <option>Brochure / Poster</option>
                                    <option>Others</option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Next</button>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
    <?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/footer.php"); ?>
</body>
</html>