<?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/doctype.php"); ?>
<?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/css_all.php"); ?>
<?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/navbar.php"); ?>

    <div class="container white-space">
        <div class="row ">
            <div class="col-sm">
                <ul class="nav justify-content-center">
                    <li class="nav-item">
                        <a class="nav-link" href="form1.php">Retreat</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="form2.php">Date</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="form3.php">Form</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="form4.php">Meditation background</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="form5.php">Health acknowledge</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link border-bottom" href="form6.php">Payment</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row my-4">
            <div class="col-sm-8">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mb-0">Check Out</h4>
                        <small><i class="fas fa-lock"></i> Secure Checkout</small>
                        <h5 class="mt-3">Pay now</h5>
                        <form>
                            <div class="form-control">
                                <div class="custom-control custom-radio custom-control-inline ml-1">
                                    <input type="radio" class="custom-control-input" id="customCheck1" name="payment" checked>
                                    <label class="custom-control-label" for="customCheck1">
                                    Use Credit or Debit Card</label>
                                </div>
                            </div>
                            <div class="card bg-light">
                                <div class="card-body">
                                    <label>BILLING DETAILS</label>
                                    <div class="form-row">
                                        <div class="form-group col-md-8">
                                            <input type="text" class="form-control" id="" placeholder="Address">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <input type="password" class="form-control" id="" placeholder="Apt, Unit">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="inputCity" placeholder="City">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <select id="inputState" class="form-control">
                                                <option selected>Country</option>
                                                <option>...</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                        <select id="inputState" class="form-control">
                                            <option selected>State</option>
                                            <option>...</option>
                                        </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                        <input type="text" class="form-control" id="inputZip" placeholder="Postal code">
                                        </div>
                                    </div>

                                    <label class="mt-3">PAYMENT DETAILS</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="inputCardholderName" placeholder="Cardholder Name">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="inputCardNumber" placeholder="Card Number">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <input type="text" class="form-control" id="" placeholder="MM/YY">
                                        </div>
                                        <div class="form-group col-md-7">
                                            <input type="text" class="form-control" id="" placeholder="CVC">
                                        </div>
                                        <div class="form-group col-md-1">
                                            <i class="far fa-question-circle my-2"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-control">
                                <div class="custom-control custom-radio custom-control-inline ml-1">
                                    <input type="radio" class="custom-control-input" id="customCheck2" name="payment">
                                    <label class="custom-control-label" for="customCheck2">
                                    Use <img src="/mmiretreat/img/PayPal_icon.png" height="15"></label>
                                </div>
                            </div>
                            <div class="card bg-light">
                                <div class="card-body text-center">
                                    <a href="" class="btn btn-primary"><img src="/mmiretreat/img/PayPal_icon-wt.png" height="20"> Checkout </a>
                                </div>
                            </div>
                            <div class="mt-4 text-center">
                                <p>By confirming, you agree to The Middle Way’s Terms of Use and Privacy Policy</p>
                                <a href="" class="btn btn-block btn-primary">CONFIRM PAYMENT </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="card bg-light">
                    <div class="card-body">
                        <p class="card-title font-weight-bold">BOOKING DETAILS</p>
                        <div class="row">
                            <div class="col-sm">
                                <div class="form-row">
                                    <div class="col-10">
                                        <label class="font-weight-bold">3-Day Meditation Retreat At POP House</label>
                                    </div>
                                    <div class="col-2">
                                        <label>$150</label>
                                    </div>
                                </div>
                                <div class="form-row border-bottom">
                                    <div class="col-10">
                                        <label class="">Subtotal</label>
                                    </div>
                                    <div class="col-2">
                                        <label>$150</label>
                                    </div>
                                </div>
                                <div class="form-row font-weight-bold">
                                    <div class="col-10">
                                        <label class="">Total</label>
                                    </div>
                                    <div class="col-2">
                                        <label class="">$150</label>
                                    </div>
                                </div>

                                <div class="form-row mt-3">
                                    <div class="col-sm">
                                        <label class="text-primary">Coupon Cod if applicable</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="text" class="form-control" id="" placeholder="Coupon Code">
                                            </div>
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-secondary" type="button" id="inputGroupFileAddon04">APPLY</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <p class="font-italic mt-2">Coupon:</p>
                                <p class="font-weight-bold my-4">WHAT INCLUDE IN THE RETREAT</p>

                                <div class="form-row font-weight-bold">
                                    <div class="col-8">
                                        <label class="">Accommodation</label>
                                        <small class="font-italic">Accommodation at the retreat for 2 nights + 3 days</small>
                                    </div>
                                    <div class="col-4 text-right font-italic">
                                        <label>INCLUDE</label>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-row font-weight-bold">
                                    <div class="col-8">
                                        <label class="">Breakfast, lunch, And refreshments</label>
                                        <small class="font-italic">Meals and refreshments Drinking</small>
                                    </div>
                                    <div class="col-4 text-right font-italic">
                                        <label>INCLUDE</label>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-row font-weight-bold">
                                    <div class="col-8">
                                        <label class="">Course materials</label>
                                        <small class="font-italic">Learning materials during Course are provided</small>
                                    </div>
                                    <div class="col-4 text-right font-italic">
                                        <label>INCLUDE</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/footer.php"); ?>
</body>
</html>