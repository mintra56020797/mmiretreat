<?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/doctype.php"); ?>
<?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/css_all.php"); ?>
<?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/navbar.php"); ?>

    <div class="container">
        <div class="row white-space">
            <div class="col-sm">
                <ul class="nav justify-content-center">
                    <li class="nav-item">
                        <a class="nav-link border-bottom" href="form1.php">Retreat</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="form2.php">Date</a>
                    </li>
                </ul>
                <h3 class="text-center my-4">Choose a retreat that you love.</h3>
                <div>
                    <div class="card-deck">
                        <div class="card">
                            <img src="/mmiretreat/img/p3.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                            <h5 class="card-title">3-Day Meditation Retreat for Beginner</h5>
                            <p class="card-text">Gain real experiences from the real practice At one of our best retreat places in Thailand. This retreat is best for people from all walk of lives. Take sometimes, it’s just great to be time off.</p>
                            <p class="card-text"><small class="text-muted">$150</small></p>
                            <a href="#" class="btn btn-primary">Select</a>
                            </div>
                        </div>
                        <div class="card">
                            <img src="/mmiretreat/img/p2.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                            <h5 class="card-title">7-Day Meditation Retreat for Beginner</h5>
                            <p class="card-text">Gain real experiences from the real practice At one of our best retreat places in Thailand. This retreat is best for people from all walk of lives. Take sometimes, it’s just great to be time off.</p>
                            <p class="card-text"><small class="text-muted">$450</small></p>
                            <a href="#" class="btn btn-primary">Select</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/footer.php"); ?>
</body>
</html>