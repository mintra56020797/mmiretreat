<?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/doctype.php"); ?>
<?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/css_all.php"); ?>
<?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/navbar.php"); ?>

    <div class="container">
        <div class="row white-space">
            <div class="col-sm">
                <ul class="nav justify-content-center">
                    <li class="nav-item">
                        <a class="nav-link" href="form1.php">Retreat</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link border-bottom" href="form2.php">Date</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="form3.php">Form</a>
                    </li>
                </ul>
                <h3 class="text-center my-4">Select the date of your retreat</h3>
                <div>
                    <div class="card-deck">
                        <div class="card shadow">
                            <img src="/mmiretreat/img/p3.jpg" class="card-img-top" alt="...">
                            <div class="card-body ">
                                <h5 class="card-title">3-Day Meditation Retreat for Beginner</h5>
                                <p class="card-text">Gain real experiences from the real practice At one of our best retreat places in Thailand. This retreat is best for people from all walk of lives. Take sometimes, it’s just great to be time off.</p>
                                <p class="card-text"><small class="text-muted">$150</small></p>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="shadow p-3 mb-5 bg-white rounded">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" >
                                    <label class="form-check-label" for="exampleRadios1">
                                    11-13 June, 2019
                                    </label>
                                </div>
                            </div>
                            <div class="shadow p-3 mb-5 bg-white rounded">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" >
                                    <label class="form-check-label" for="exampleRadios1">
                                    11-13 June, 2019
                                    </label>
                                </div>
                            </div>
                            <div class="shadow p-3 mb-5 bg-white rounded">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" >
                                    <label class="form-check-label" for="exampleRadios1">
                                     17-19 October, 2019
                                    </label>
                                </div>
                            </div>
                            <div class="shadow p-3 mb-5 bg-white rounded">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" >
                                    <label class="form-check-label" for="exampleRadios1">
                                    10- 12 December, 2019
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <?php include($_SERVER["DOCUMENT_ROOT"]."/mmiretreat/includes/footer.php"); ?>
</body>
</html>